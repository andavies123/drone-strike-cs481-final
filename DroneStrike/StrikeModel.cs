﻿using System;
using System.Collections.Generic;

using Newtonsoft.Json;

namespace DroneStrike {

    public partial class StrikeModel {

        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("strike")]
        public List<Strike> Strikes { get; set; }
    }

    public partial class Strike {
        [JsonProperty("_id")]
        public string Id { get; set; }

        [JsonProperty("number")]
        public long Number { get; set; }

        [JsonProperty("country")]
        public string Country { get; set; }

        [JsonProperty("date")]
        public DateTimeOffset Date { get; set; }

        [JsonProperty("narrative")]
        public string Narrative { get; set; }

        [JsonProperty("town")]
        public string Town { get; set; }

        [JsonProperty("location")]
        public string Location { get; set; }

        [JsonProperty("deaths")]
        public string Deaths { get; set; }

        [JsonProperty("deaths_min")]
        public string DeathsMin { get; set; }

        [JsonProperty("deaths_max")]
        public string DeathsMax { get; set; }

        [JsonProperty("civilians")]
        public string Civilians { get; set; }

        [JsonProperty("injuries")]
        public string Injuries { get; set; }

        [JsonProperty("children")]
        public string Children { get; set; }

        [JsonProperty("tweet_id")]
        public string TweetId { get; set; }

        [JsonProperty("bureau_id")]
        public string BureauId { get; set; }

        [JsonProperty("bij_summary_short")]
        public string BijSummaryShort { get; set; }

        [JsonProperty("bij_link")]
        public string BijLink { get; set; }

        [JsonProperty("target")]
        public string Target { get; set; }

        [JsonProperty("lat")]
        public string Lat { get; set; }

        [JsonProperty("lon")]
        public string Lon { get; set; }

        [JsonProperty("articles")]
        public List<object> Articles { get; set; }

        [JsonProperty("names")]
        public List<string> Names { get; set; }

        public int MaxDeaths { get; set; }
    }
}
