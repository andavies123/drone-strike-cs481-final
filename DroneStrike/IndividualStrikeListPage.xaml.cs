﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using Xamarin.Forms;

namespace DroneStrike {
    public partial class IndividualStrikeListPage : ContentPage {

        List<Strike> Strikes { get; set; }

        bool isDefaultSort = true;

        public IndividualStrikeListPage() {
            InitializeComponent();
            dataPicker.ItemsSource = new String[] { "Date", "Deaths", "Country" };
            orderPicker.ItemsSource = new String[] { "Oldest", "Newest" };
        }

        public void UpdateModel(StrikeModel model) {
            Strikes = model.Strikes;
            UpdateList();
            dataPicker.SelectedIndex = 0;
            orderPicker.SelectedIndex = 0;
        }

        public void UpdateList() {
            listView.ItemsSource = new ObservableCollection<Strike>(Strikes);
        }

        void OnMoreInfo(object sender, EventArgs e) {
            Navigation.PushAsync(new IndividualTabbedPage((sender as MenuItem).CommandParameter as Strike));
        }

        void PickerIndexChanged(object sender, System.EventArgs e) {
            Picker picker = sender as Picker;
            if(picker == dataPicker) {
                switch(picker.SelectedIndex) {
                    case 0:
                        Strikes.Sort((x, y) => x.Date.CompareTo(y.Date));
                        orderPicker.ItemsSource = new String[] { "Oldest", "Newest" };
                        break;
                    case 1:
                        //Strikes.Sort((x, y) => string.Compare(x.DeathsMax, y.DeathsMax, StringComparison.CurrentCulture));
                        SortListByDeaths();
                        orderPicker.ItemsSource = new String[] { "Lowest", "Highest" };
                        break;
                    case 2:
                        Strikes.Sort((x, y) => string.Compare(x.Country, y.Country, StringComparison.CurrentCulture));
                        orderPicker.ItemsSource = new String[] { "A - Z", "Z - A" };
                        break;
                }
                isDefaultSort = true;
                orderPicker.SelectedIndex = 0;
                UpdateList();
            }
            else if(picker == orderPicker) {
                switch(picker.SelectedIndex) {
                    case 0:
                        if(!isDefaultSort) {
                            Strikes.Reverse();
                            isDefaultSort = true;
                        }
                        break;
                    case 1: 
                        if(isDefaultSort) {
                            Strikes.Reverse();
                            isDefaultSort = false;
                        }
                        break;
                }
                UpdateList();
            }
        }

        void SortListByDeaths() {
            for(int index = 0; index < Strikes.Count; index++) {
                string death = Strikes[index].DeathsMax;
                if (Int32.TryParse(death, out int output)) Strikes[index].MaxDeaths = output;
                else Strikes[index].MaxDeaths = -1;
            }
            Strikes.Sort((x, y) => x.MaxDeaths.CompareTo(y.MaxDeaths));
        }
    }
}
