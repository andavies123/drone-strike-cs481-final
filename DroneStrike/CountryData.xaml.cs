﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;
using Microcharts;
using Microcharts.Forms;
using Entry = Microcharts.Entry;
using System.Diagnostics;

namespace DroneStrike {

    public class ItemsPerCountry {
        public string country;
        public int items;
    }

    public partial class CountryData : ContentPage {

        List<Strike> Strikes { get; set; }
        List<ItemsPerCountry> data = new List<ItemsPerCountry>();

        public CountryData(List<Strike> strikes) {
            Strikes = strikes;
            InitializeComponent();
            ParseData();
            CreateBarChart();
        }

        void ParseData() {
            foreach(Strike strike in Strikes) {
                if (data.Count == 0) data.Add(new ItemsPerCountry { country = strike.Country, items = 0 });
                int countryIndex = -1;
                int result = 0;
                for(int index = 0; index < data.Count; index++) {
                    if (strike.Country == data[index].country) countryIndex = index;
                }
                if (countryIndex != -1) {
                    if (int.TryParse(strike.DeathsMax, out result)) data[countryIndex].items += result;
                }
                else {
                    if (strike.Country == "Pakistan-Afghanistan Border") data[0].items += result;
                    else data.Add(new ItemsPerCountry { country = strike.Country, items = result });
                }
            }
        }

        void CreateBarChart() {
            Entry[] entries = new Entry[data.Count];
            for(int index = 0; index < entries.Length; index++) {
                entries[index] = new Entry(data[index].items) {
                    Label = data[index].country,
                    ValueLabel = data[index].items.ToString(),
                    Color = SkiaSharp.SKColor.Parse("#BF0A30"),
                    TextColor = SkiaSharp.SKColor.Parse("#FFFFFF")
                };
            }
            chartView.Chart = new BarChart() { 
                Entries = entries, 
                BackgroundColor = SkiaSharp.SKColor.Parse("#002868"),
                LabelTextSize=35
            };
        }
    }
}
