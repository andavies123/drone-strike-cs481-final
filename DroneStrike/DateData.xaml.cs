﻿using System;
using System.Collections.Generic;
using Microcharts;
using Microcharts.Forms;
using Entry = Microcharts.Entry;

using Xamarin.Forms;

namespace DroneStrike {
    public partial class DateData : ContentPage {

        public class ItemsPerCountry {
            public string year;
            public int items;
        }

        List<Strike> Strikes { get; set; }
        List<ItemsPerCountry> data = new List<ItemsPerCountry>();

        public DateData(List<Strike> strikes) {
            Strikes = strikes;
            InitializeComponent();
            ParseData();
            CreateBarChart();
        }

        void ParseData() {
            foreach (Strike strike in Strikes) {
                if (data.Count == 0) data.Add(new ItemsPerCountry { year = strike.Date.Year.ToString(), items = 0 });
                int yearIndex = -1;
                int result = 0;
                for (int index = 0; index < data.Count; index++) {
                    if (strike.Date.Year.ToString() == data[index].year) yearIndex = index;
                }
                int.TryParse(strike.DeathsMax, out result);
                if (yearIndex != -1) data[yearIndex].items += result;
                else  data.Add(new ItemsPerCountry { year = strike.Date.Year.ToString(), items = result }); 
            }
        }

        void CreateBarChart() {
            Entry[] entries = new Entry[data.Count];
            for (int index = 0; index < entries.Length; index++) {
                entries[index] = new Entry(data[index].items) {
                    Label = data[index].year.Substring(2, 2),
                    ValueLabel = data[index].items.ToString(),
                    Color = SkiaSharp.SKColor.Parse("#BF0A30"),
                    TextColor = SkiaSharp.SKColor.Parse("#FFFFFF")
                };
            }
            chartView.Chart = new BarChart() { 
                Entries = entries, 
                LabelTextSize = 22,
                BackgroundColor = SkiaSharp.SKColor.Parse("#002868")
            };
        }
    }
}
