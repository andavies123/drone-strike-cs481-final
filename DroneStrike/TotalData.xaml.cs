﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace DroneStrike {
    public partial class TotalData : ContentPage {

        List<Strike> Strikes { get; set; }

        int totalDeaths = 0;
        int totalCivilians = 0;
        int totalChildren = 0;
        int totalInjuries = 0;

        public TotalData(List<Strike> strikes) {
            Strikes = strikes;
            UpdateData();
            InitializeComponent();
            UpdateLabels();
        }

        void UpdateData() {
            foreach(Strike strike in Strikes) {
                if (int.TryParse(strike.DeathsMax, out int deathsResult)) totalDeaths += deathsResult;
                if (int.TryParse(strike.Civilians, out int civiliansResult)) totalCivilians += civiliansResult;
                if (int.TryParse(strike.Children, out int childrenResult)) totalChildren += childrenResult;
                if (int.TryParse(strike.Injuries, out int injuriesResult)) totalInjuries += injuriesResult;
            }
        }

        void UpdateLabels() {
            totalDeathsLabel.Text = "Total Deaths - " + totalDeaths.ToString();
            totalCiviliansLabel.Text = "Total Civilian Deaths - " + totalCivilians.ToString();
            totalChildrenLabel.Text = "Total Children Deaths - " + totalChildren.ToString();
            totalInjuriesLabel.Text = "Total Injuries - " + totalInjuries.ToString(); 
        }
    }
}
