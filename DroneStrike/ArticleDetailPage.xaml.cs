﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace DroneStrike {
    public partial class ArticleDetailPage : ContentPage {

        public Article CurrentArticle { get; set; }

        public ArticleDetailPage(Article article) {
            CurrentArticle = article;
            BindingContext = this;
            InitializeComponent();
        }
    }
}
