﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Xamarin.Forms;

namespace DroneStrike {
    public partial class NewsListPage : ContentPage {
        public NewsListPage() {
            InitializeComponent();
        }

        public void UpdateList(NewsModel newsModel) {
            listView.ItemsSource = new ObservableCollection<Article>(newsModel.Articles);
        }

        void OnMoreInfo(object sender, EventArgs e) {
            Navigation.PushAsync(new ArticleDetailPage((sender as MenuItem).CommandParameter as Article));
        }
    }
}
