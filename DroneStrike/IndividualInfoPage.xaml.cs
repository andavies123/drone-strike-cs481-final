﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Xamarin.Forms;

namespace DroneStrike {
    public partial class IndividualInfoPage : ContentPage {

        public Strike CurrentStrike { get; set; }

        public IndividualInfoPage(Strike strike) {
            CurrentStrike = strike;
            BindingContext = this;
            InitializeComponent();
        }
    }
}
