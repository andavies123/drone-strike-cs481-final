﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Xamarin.Forms;
using Xamarin.Forms.Maps;

namespace DroneStrike {
    public partial class IndividualMapPage : ContentPage {

        public Strike CurrentStrike { get; set; }
        public Position StrikePosition { get; set; }

        public IndividualMapPage(Strike strike) {
            CurrentStrike = strike;
            StrikePosition = new Position(Double.Parse(CurrentStrike.Lat), Double.Parse(CurrentStrike.Lon));
            BindingContext = this;

            InitializeComponent();

            droneMap.MoveToRegion(new MapSpan(StrikePosition, 0.005, 0.005));
            droneMap.Pins.Add(new Pin() {
                Position = StrikePosition,
                Label = CurrentStrike.Location
            });
        }
    }
}
