﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Xamarin.Forms;

namespace DroneStrike {
    public partial class TotalStrikesListPage : ContentPage {

        List<Strike> Strikes { get; set; }

        public TotalStrikesListPage() {
            InitializeComponent();
        }

        public void UpdateData(List<Strike> strikes) { Strikes = strikes; }

        void ButtonPressed(object sender, System.EventArgs e) {
            Button button = sender as Button;
            if(button == countryButton) Navigation.PushAsync(new NavigationPage(new CountryData(Strikes)) { BarBackgroundColor = Color.FromHex("#002868") });
            else if(button == dateButton) Navigation.PushAsync(new NavigationPage(new DateData(Strikes)) { BarBackgroundColor = Color.FromHex("#002868") });
            else if(button == totalButton) Navigation.PushAsync(new NavigationPage(new TotalData(Strikes)) { BarBackgroundColor = Color.FromHex("#002868") });
        }
    }
}
