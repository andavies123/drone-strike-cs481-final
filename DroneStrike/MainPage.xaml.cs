﻿using System;
using System.Diagnostics;
using System.Net.Http;
using Newtonsoft.Json;
using Xamarin.Forms;

namespace DroneStrike {
    public partial class MainPage : MasterDetailPage {

        private string droneAPIAddress = "http://api.dronestre.am/data";
        private string newsAPIAddress = "https://newsapi.org/v2/everything?q=drone%20strike&apiKey=17a7e104582745b48c87a9ad54a2daad";

        private IndividualStrikeListPage individualStrikeListPage = new IndividualStrikeListPage();
        private TotalStrikesListPage totalStrikesPage = new TotalStrikesListPage();
        private NewsListPage newsListPage = new NewsListPage();

        public MainPage() {
            InitializeComponent();
            SetDetailPage(new NavigationPage(individualStrikeListPage));
            GetDroneInfo();
            GetNewsInfo();
        }

        void ButtonPressed(object sender, System.EventArgs e) {
            if (sender as Button == individualPageButton)
                SetDetailPage(new NavigationPage(individualStrikeListPage));
            else if (sender as Button == totalPageButton)
                SetDetailPage(new NavigationPage(totalStrikesPage));
            else if (sender as Button == newsPageButton)
                SetDetailPage(new NavigationPage(newsListPage));
            IsPresented = false;
        }

        void SetDetailPage(NavigationPage navigationPage)
        {
            navigationPage.BarTextColor = Color.FromHex("#FFFFFF");
            navigationPage.BarBackgroundColor = Color.FromHex("#002868");
            Detail = navigationPage;
        }

        async void GetDroneInfo() {
            HttpClient client = new HttpClient();
            Uri uri = new Uri(droneAPIAddress);
            StrikeModel strikeData = new StrikeModel();

            HttpResponseMessage response = await client.GetAsync(uri);
            if(response.IsSuccessStatusCode) {
                string jsonContent = await response.Content.ReadAsStringAsync();
                strikeData = JsonConvert.DeserializeObject<StrikeModel>(jsonContent);
                individualStrikeListPage.UpdateModel(strikeData);
                totalStrikesPage.UpdateData(strikeData.Strikes);
            }
        }

        async void GetNewsInfo() {
            HttpClient client = new HttpClient();
            Uri uri = new Uri(newsAPIAddress);
            NewsModel newsData = new NewsModel();

            HttpResponseMessage response = await client.GetAsync(uri);
            if(response.IsSuccessStatusCode) {
                string jsonContent = await response.Content.ReadAsStringAsync();
                newsData = JsonConvert.DeserializeObject<NewsModel>(jsonContent);
                newsListPage.UpdateList(newsData);
            }
        }
    }
}
