﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace DroneStrike {
    public partial class IndividualTabbedPage : TabbedPage {

        public IndividualTabbedPage(Strike strike) {
            Children.Add(new IndividualInfoPage(strike));
            Children.Add(new IndividualMapPage(strike));
            InitializeComponent();
            this.BarBackgroundColor = Color.FromHex("#002868");
        }
    }
}
